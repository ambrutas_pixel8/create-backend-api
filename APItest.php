<?php

require_once 'API.php';
use PHPUnit\Framework\TestCase;

class APITest extends TestCase
{
    protected function setUp(): void
    {
        $this->api = new API();
    }


    public function testHttpPost()
    {
        $_SERVER['REQUEST_METHOD'] = 'POST';


        $payload = array(
            'first_name' => 'Test',
            'middle_name' => 'test',
            'last_name' => 'last test',
            'contact_number' => 654655
        );
        $result = json_decode($this->api->httpPost($payload), true);
        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'success');
        $this->assertArrayHasKey('data', $result);
    }

    public function testHttpPut()
    {
        // Test code for PUT method
        $_SERVER['REQUEST_METHOD'] = 'PUT';

    $id = 1; // Specify the ID for update
    $payload = array(
        'id' => 1,
        'first_name' => 'Updated First Name',
        'middle_name' => 'Updated Middle Name',
        'last_name' => 'Updated Last Name',
        'contact_number' => 12345678
    );

    $response = $this->api->httpPut($id, $payload);
    $result = json_decode($response, true);

    $this->assertArrayHasKey('status', $result);

    if ($id == $payload['id']) {
        $this->assertEquals($result['status'], 'success');
        $this->assertArrayHasKey('data', $result);
        $this->assertEquals($result['data'], $payload);
    } else {
        $this->assertEquals($result['status'], 'failed');
        $this->assertArrayNotHasKey('data', $result);
    }
    }

    public function testHttpDelete()
    {
        // Test code for DELETE method
        $_SERVER['REQUEST_METHOD'] = 'DELETE';

        $id = 1;
        $payload = array(
        'id' => 1
        );
        
        $result = json_decode($this->api->httpDelete($id,$payload), true);
        $this->assertArrayHasKey('status', $result);

        if ($id == $payload['id']) {
        $this->assertEquals($result['status'], 'success');
        $this->assertArrayHasKey('data', $result);
        } else {
            $this->assertEquals($result['status'], 'failed');
            $this->assertArrayNotHasKey('data', $result);
        }
    }

    public function testHttpGet()
    {
        // Test code for GET method
        $_SERVER['REQUEST_METHOD'] = 'GET';

        $id = 2; // The ID of the resource to retrieve
        $result = json_decode($this->api->httpGet($id), true);
        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'success');
        $this->assertArrayHasKey('data', $result);
    }
}


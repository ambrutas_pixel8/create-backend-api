<?php

// Tells the browser to allow code from any origin to access
header("Access-Control-Allow-Origin: *");
// Tells browsers whether to expose the response to the frontend JavaScript code
header("Access-Control-Allow-Credentials: true");
// Specifies one or more methods allowed when accessing a resource in response to a preflight request
header("Access-Control-Allow-Methods: POST, GET, PUT, DELETE");
// Used in response to a preflight request which includes the Access-Control-Request-Headers to indicate which HTTP headers can be used during the actual request
header("Access-Control-Allow-Headers: Content-Type");

require_once('MysqliDb.php'); // Include thingEngineer file

//API Class
class API {

    public function __construct() // Constructor function that sets up a connection to a MySQL database using the MysqliDB class
    {
        $this->db = new MysqliDb('localhost' , 'root' , '' , 'employee');
    }

    public function httpGet($payload) {
        // Retrieve the 'id' parameter from the payload
        $id = isset($payload['id']) ? $payload['id'] : null;
        // If 'id' is provided, add a WHERE condition to the query
        if ($id) {
            $this->db->where('id', $id);
        }
        // Retrieve data from the 'information' table based on the query
        $results = $this->db->get('information');
        // Check if the query returned any results
        if (!empty($results)) {
            // Return a success response with the retrieved data
            return json_encode(array(
                'method' => 'GET',
                'status' => 'success',
                'data' => $results
            ));
        } else {
            // Return a failure response if no data is found
            return json_encode(array(
                'method' => 'GET',
                'status' => 'failed',
                'message' => 'Failed to fetch data'
            ));
        }
    }
        
    public function httpPost($payload) {
        // Check if the payload is an array and not empty
        if (is_array($payload) && !empty($payload)) {
            // Execute a query using the MysqliDB class to insert data into the database
            $result = $this->db->insert('information', $payload);
    
            // Check if the query was successful
            if ($result) {
                return json_encode(array(
                    'method' => 'POST',
                    'status' => 'success',
                    'data' => $payload
                ));
            } else {
                return json_encode(array(
                    'method' => 'POST',
                    'status' => 'failed',
                    'message' => 'Failed to Insert Data'
                ));
            }
        } else {
            // Return an error response for invalid payload
            return json_encode(array(
                'method' => 'POST',
                'status' => 'failed',
                'message' => 'Invalid payload'
            ));
        }
    }

    public function httpPut($id, $payload) {
        if (!empty($id) && !empty($payload)) {
            $payloadId = isset($payload['id']) ? $payload['id'] : null;
    
            if ($id == $payloadId) {
                $this->db->where('id', $id);
                $result = $this->db->update('information', $payload);
    
                if ($result) {
                    return json_encode(array(
                        'method' => 'PUT',
                        'status' => 'success',
                        'data' => $payload
                    ));
                } else {
                    return json_encode(array(
                        'method' => 'PUT',
                        'status' => 'failed',
                        'message' => 'Failed to Update Data'
                    ));
                }
            }
        }
        return json_encode(array(
            'method' => 'PUT',
            'status' => 'failed',
            'message' => 'Invalid Request'
        ));
    }
    
    public function httpDelete($id, $payload)
    {
        //check if id and payload are not null or empty
        if(!empty($id) && !empty($payload)) {
            //check if id passed matches the id in paylaod   
            if($id == $payload['id']){
                //additional condition to check if the payload is an array or not
                if(!is_array($payload)){
                    //use where funtion with ID condition
                    $this->db->where('id', $id);
                }else{
                    //use the where function with SQL IN operator and payload
                    $this->db->where('id', $payload, 'IN');
                }
                $result = $this->db->delete('information');

                
                //check if the query is successful
                if($result){
                    return json_encode(array(
                        'method' => 'DELETE',
                        'status' => 'success',
                        'data' => $payload
                    ));
                } else{
                    return json_encode(array(
                        'method'=> 'DELETE',
                        'status'=> 'failed',
                        'message' => 'Failed to Delete Data'
                    ));
                }
            }
        } 

        return json_encode(array(
            'method'=>'DELETE',
            'status'=> 'failed',
            'message' => 'Invalid Request'
        ));
    }
}

//identifier if what type of request
$request_method = $_SERVER['REQUEST_METHOD'];

//For GET, POST, PUT, & DELETE Request
if ($request_method === 'GET') {
    $received_data = $_GET;
} else{
    //check if method is PUT or DELETE, and get the IDs on URL
    if($request_method === 'PUT' || $request_method === 'DELETE'){
        $request_uri = $_SERVER['REQUEST_URI'];

        $id = null;

        $exploded_request_uri = array_values(explode("/", $request_uri));

        $last_index = count($exploded_request_uri) -1;

        $id = $exploded_request_uri[$last_index];
    }
}

//payload data
$received_data = json_decode(file_get_contents('php://input'), true);

$api = new API;


 //Checking if what type of request and designating to specific functions
  switch ($request_method) {
      case 'GET':
          echo $api->httpGet($received_data);
          break;
      case 'POST':
          echo $api->httpPost($received_data);
          break;
      case 'PUT':
          echo $api->httpPut($id, $received_data);
          break;
      case 'DELETE':
          echo $api->httpDelete($id, $received_data);
          break;
  }

?>
